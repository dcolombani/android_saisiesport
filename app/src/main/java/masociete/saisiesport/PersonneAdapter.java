package masociete.saisiesport;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class PersonneAdapter extends ArrayAdapter<Personne> {

	ArrayList<Personne> LPersonne;
	Context context;
	int viewRes;

	public PersonneAdapter(Context context, int textViewResourceId,
                           ArrayList<Personne> inscrits) {
		super(context, textViewResourceId, inscrits);
		this.LPersonne = inscrits;
		this.context = context;
		this.viewRes = textViewResourceId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			LayoutInflater vi =
					(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(viewRes, parent, false);
		}
		final Personne p = LPersonne.get(position);
		if (p != null) {
			TextView tt = (TextView) v.findViewById(R.id.title);
			TextView td = (TextView) v.findViewById(R.id.description);
            Resources res = context.getResources();

            if (tt != null) {

				tt.setText(res.getString(R.string.nom) + " " + p.getNom());
			}
			if (td != null) {
				td.setText(res.getString(R.string.age) +" " + p.getAge());
			}
			ImageView mImageView = v.findViewById(R.id.icon);
			if (p.getPhoto() !=null) mImageView.setImageBitmap(p.getPhoto());
			Button b = v.findViewById(R.id.bRemove);
			b.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					Saisie.Liste.remove(p);
					PersonneAdapter.this.notifyDataSetChanged();
				}
			});
		}
		return v;
	}
}
