package masociete.saisiesport;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.provider.ContactsContract;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;

public class Saisie {
    public static ArrayList<Personne> Liste = new ArrayList<>();

    public static void Enregistre(String fichier, Context c) {
        PrintStream ps = null;
        try {
            FileOutputStream f = c.openFileOutput(fichier, Context.MODE_PRIVATE);
            ps = new PrintStream(f);
            for (Personne p : Liste) {
                p.sauve(ps);
                if (p.getPhoto() != null) {
                    FileOutputStream out = c.openFileOutput(p.getNom()+".png", Context.MODE_PRIVATE);
                    p.getPhoto()
                            .compress(Bitmap.CompressFormat.PNG, 100, out);
                    out.close();
                }
            }
        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            if (ps != null) ps.close();
        }
    }

    public static void Relit(String fichier, Context c) {
        try {
            FileInputStream f = c.openFileInput(fichier);
            InputStreamReader sr = new InputStreamReader(f);
            BufferedReader b = new BufferedReader(sr);
            String nom;
            Liste.clear();
            while ((nom = b.readLine()) != null) {
                int age = Integer.parseInt(b.readLine());
                Personne p = new Personne(nom, age);
                Liste.add(p);
                String NomPhoto = p.getNom() + ".png";
                File reper = c.getFilesDir();
                File fichierPhoto = new File(reper.getAbsolutePath()+"/"+NomPhoto);
                if (fichierPhoto.exists()) {
                    FileInputStream photoStream = c.openFileInput(NomPhoto);
                    p.setPhoto(BitmapFactory.decodeStream(photoStream));
                }
            }
            b.close();
        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (PersonneException e) {
            e.printStackTrace();
        }
    }
}
