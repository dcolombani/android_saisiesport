package masociete.saisiesport;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TextView;

public class AffichePersonne extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_affiche_personne);
        Intent i = getIntent();
        Personne p = i.getParcelableExtra(Personne.P);
        TextView nom = findViewById(R.id.txtNom);
        nom.setText(p.getNom());
        TextView age = findViewById(R.id.txtAge);
        // age.setText(p.getAge()); Ne surtout pas faire !!!!
        age.setText(Integer.toString(p.getAge()) + " ans");
    }
}

