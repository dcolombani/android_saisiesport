package masociete.saisiesport;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener,
        DialogInterface.OnClickListener,
        TextWatcher {
    final static int CAPTURE_PHOTO = 1;
    EditText SaisieAge;
    EditText SaisieNom;
    Bitmap photoBitMap = null;
    Personne p;
    ImageView mImageView;
    AlertDialog d;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SaisieAge = findViewById(R.id.saisieAge);
        SaisieAge.addTextChangedListener(this);
        SaisieNom = findViewById(R.id.saisieNom);
        Button b = findViewById(R.id.bAjout);
        b.setOnClickListener(this);
        Button PrendPhoto = findViewById(R.id.prendPhoto);
        PrendPhoto.setOnClickListener(this);
        Button bAff = findViewById(R.id.bAffiche);
        bAff.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bAjout:
                final EditText SaisieNom = findViewById(R.id.saisieNom);
                try {
                    int age = Integer.parseInt(SaisieAge.getText().toString());
                    p = new Personne(SaisieNom.getText().toString(), age);
                    p.setPhoto(photoBitMap);
                    if (Saisie.Liste.contains(p)) {
                        Toast.makeText(MainActivity.this, p.toString() + "est déjà enregistré", Toast.LENGTH_LONG).show();
                    } else {
                        LayoutInflater in = MainActivity.this.getLayoutInflater();
                        View v1 = in.inflate(R.layout.confirm_layout, null);
                        TextView nm = v1.findViewById(R.id.title);
                        mImageView = v1.findViewById(R.id.icon);
                        if (p.getPhoto() !=null) mImageView.setImageBitmap(p.getPhoto());
                        nm.setText(p.getNom());
                        TextView ag = v1.findViewById(R.id.description);
                        // age.setText(p.getAge()); Ne surtout pas faire !!!!
                        ag.setText(Integer.toString(p.getAge()) + " ans");
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        builder.setView(v1);
                        builder.setTitle("Confirmez la saisie");
                        builder.setPositiveButton("Valider", this);
                        builder.setNegativeButton("Refuser", this);
                        d = builder.create();
                        d.show();

                    }
                } catch (PersonneException e) {
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
                catch (NumberFormatException e) {
                    Toast.makeText(MainActivity.this, "L'age n'est pas correctement défini", Toast.LENGTH_LONG).show();
                }
                catch (Exception e) {
                    Log.d("Sortie", e.getMessage());
                }
                break;
            case R.id.bAffiche:
                Intent i = new Intent(MainActivity.this, ListeActivity.class);
                startActivity(i);
                break;
            case R.id.prendPhoto:
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, CAPTURE_PHOTO);
                }
                break;
        }

    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i) {
            case DialogInterface.BUTTON_POSITIVE:
                Saisie.Liste.add(p);
                SaisieAge.setText("");
                SaisieNom.setText("");
                mImageView.setImageBitmap(null);
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                break;
        }
        d = null;

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_PHOTO && resultCode == RESULT_OK) {
            mImageView = findViewById(R.id.Photo);
            Bundle extras = data.getExtras();
            photoBitMap = (Bitmap) extras.get("data");
            mImageView.setImageBitmap(photoBitMap);
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("photo", photoBitMap);
        if (d != null) {
            d.dismiss();
            outState.putBoolean("dialog", true);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mImageView = findViewById(R.id.Photo);
        photoBitMap = (Bitmap) savedInstanceState.get("photo");
        if (photoBitMap != null) mImageView.setImageBitmap(photoBitMap);
        if (savedInstanceState.getBoolean("dialog", false)) {
            Button b = findViewById(R.id.bAjout);
            b.performClick();
        }
    }

    @Override
    protected void onStop() {
        Saisie.Enregistre("sauvegarde.txt", this);
        super.onStop();
    }
    @Override
    protected void onStart() {
        Saisie.Relit("sauvegarde.txt", this);
        super.onStart();
    }
    static void test() {
        try {
            Personne p1 = new Personne("toto", 30);
            Log.d("Sortie", p1.getNom() + " : " + p1.getAge() + " ans");
            p1.setNom("Starck");
            p1.setAge(20);
            Log.d("Sortie", p1.getNom() + " : " + p1.getAge() + " ans");
            return;
        } catch (Exception e) {
            Log.d("Sortie", "Le nom ne peut pas être vide");
        } finally {
            Log.d("Sortie", "Code toujours exécuté");
        }
    }

    static void testArrayList() {
        ArrayList<Integer> collectionEntier = new ArrayList<Integer>();
        Log.d("Sortie", Integer.toString(collectionEntier.size()));
        collectionEntier.add(10);
        collectionEntier.add(20);
        Log.d("Sortie", Integer.toString(collectionEntier.size()));
        Log.d("Sortie", Integer.toString(collectionEntier.get(0)));
        for (Integer x : collectionEntier) {
            Log.d("Sortie-boucle", x.toString());
        }
        collectionEntier.remove((Integer) 10);
        Log.d("Sortie", Integer.toString(collectionEntier.size()));
        Log.d("Sortie", Integer.toString(collectionEntier.get(0)));

    }
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

        int nbChar = SaisieAge.getText().toString().length();
        if (nbChar > 2) {
            Snackbar.make(SaisieAge,"L'age doit être inférieur à 100",Snackbar.LENGTH_LONG)
                    .show();
        }
    }

}
