package masociete.saisiesport;

import android.graphics.Bitmap;
import android.os.Parcel;
import android.os.Parcelable;

import java.io.PrintStream;
import java.util.Objects;

public class Personne implements Parcelable {
    public static final String P ="Personne";
    private String nom;
    private Bitmap photo;
    private int age;
    public Bitmap getPhoto() { return photo;}
    public void setPhoto(Bitmap photo) { this.photo = photo;}
    public String getNom() { return nom;}
    public void setNom(String nom) throws PersonneException {
        if (nom.isEmpty()) {
            throw new PersonneException("Le nom ne peut pas être vide");
        } else {
            this.nom = nom;
        }
    }
    public int getAge() { return age;}
    public void setAge(int age) {
        // todo  : ajouter un test de validité
        this.age = age;
    }
    public Personne(String nom, int age) throws PersonneException {
        setNom(nom);
        setAge(age);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Personne personne = (Personne) o;
        return age == personne.age &&
                Objects.equals(nom, personne.nom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nom, age);
    }

    @Override
    public String toString() {
        return "Personne{"+ nom + ", " + age + "} ";
    }

    public Personne(Parcel source) {
        try {
            setNom(source.readString());
            setAge(source.readInt());
        }
        catch(Exception e){
            // todo : il faudrait faire quelque chose
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getNom());
        dest.writeInt(getAge());
    }

    public static final Parcelable.Creator<Personne> CREATOR = new Parcelable.Creator<Personne>() {

        @Override
        public Personne createFromParcel(Parcel source) {
            return new Personne(source);
        }

        @Override
        public Personne[] newArray(int size) {
            return new Personne[size];
        }
    };
    public void sauve (PrintStream p) {
        p.println(nom);
        p.println(age);
    }
}

