package masociete.saisiesport;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.content.Context.MODE_PRIVATE;

public class FragmentBottom extends Fragment implements View.OnClickListener,
        SharedPreferences.OnSharedPreferenceChangeListener {
    final public String OPERATEUR = "operateur";
    SharedPreferences Pref;
    TextView operateur;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.copyright, container, false);
        setHasOptionsMenu(true);
        Button b = (Button) v.findViewById(R.id.bSupport);
        b.setOnClickListener(this);
        operateur = (TextView) v.findViewById(R.id.operateur);
        Pref = getActivity().getSharedPreferences("donnees", MODE_PRIVATE);
        /*
        Ne pas utiliser ici une classe anonyme pour implementer le
        OnSharedPreferenceChangeListener !
        Cf  Documentation qui indique que la référence est faible.
        Le Listener peut être effacéparle GC
        https://developer.android.com/reference/android/content/SharedPreferences.html#registerOnSharedPreferenceChangeListener(android.content.SharedPreferences.OnSharedPreferenceChangeListener)
         */
        Pref.registerOnSharedPreferenceChangeListener(this);
        if (Pref.contains(OPERATEUR)) {
            operateur.setText(Pref.getString(OPERATEUR, "Inconnu"));
        } else {
            SaisieOperateur();
        }
        return v;
    }

    public void onClick(View v) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setData(Uri.fromParts("tel", "061234567", ""));
        startActivity(intent);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.appmenu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Activity a = getActivity();
        switch (item.getItemId()) {
            case R.id.quitter:
                a.moveTaskToBack(true);
                break;
            case R.id.retour:
                a.finish();
                break;
        }
        return true;
    }

    public void SaisieOperateur() {
        LayoutInflater in = getActivity().getLayoutInflater();
        final View v1 = in.inflate(R.layout.operateur, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(v1);
        builder.setTitle("Saisissez le nom de l'opérateur");
        builder.setPositiveButton("Valider", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                EditText NomOperateur = v1.findViewById(R.id.sOperateur);
                SharedPreferences.Editor ed = Pref.edit();
                ed.putString(OPERATEUR, NomOperateur.getText().toString());
                ed.commit();
            }
        });
        AlertDialog d = builder.create();
        d.show();
    }

    @Override
    public void onSharedPreferenceChanged(
            SharedPreferences sharedPreferences, String key) {
        operateur.setText(Pref.getString(OPERATEUR, "Inconnu"));
    }
}


