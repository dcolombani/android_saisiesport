package masociete.saisiesport;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class ListeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_liste);

        PersonneAdapter adapter = new PersonneAdapter(this,
                R.layout.inscrit_layout, Saisie.Liste);
        final ListView list = (ListView) findViewById(R.id.ListeInscrits);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> adapter, View v, int position,
                                    long id) {
                Personne selectedItem = (Personne) adapter.getItemAtPosition(position);
                Intent i = new Intent(ListeActivity.this,AffichePersonne.class);
                i.putExtra(Personne.P,selectedItem);
                startActivity(i);
            }
        });
    }
}
